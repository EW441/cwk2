/*
test.cpp
Author: M00663746
Created: 12/07/2021
Updated: 16/07/2021
*/

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "book.h"

TEST_CASE("Book test", "[book]")
{
  Book b1 = Book("Code Clean", "Robert Martin", "9780132350884", 1);
  std::cout << b1;
  REQUIRE(b1.getTitle()=="Code Clean");
  REQUIRE(b1.getAuthor()=="Robert Martin");
  REQUIRE(b1.getISBN()=="9780132350884");
  REQUIRE(b1.getQuantity()==1);
}
