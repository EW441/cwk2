#ifndef _BOOK_H_
#define _BOOK_H_

/*
book.h
Author: M00663746
Created: 10/07/2021
Updated: 16/07/2021
*/

#include <iostream>
#include <ostream>
#include <string>

//Book Class
class Book {
public:
  std::string title;
  std::string author;
  std::string ISBN;
  int quantity;
  Book();
  Book(std::string title, std::string author, std::string ISBN, int quantity);

  //Setter methods
  void setTitle(std::string title);
  void setAuthor(std::string author);
  void setISBN(std::string ISBN);
  void setQuantity(int quantity);

  //Getter methods
  std::string getTitle();
  std::string getAuthor();
  std::string getISBN();
  int getQuantity();

  //Friend
  friend std::ostream& operator<<(std::ostream& out, Book book);
};

#endif
