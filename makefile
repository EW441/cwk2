CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic

.PHONY : all
all : mainLibrary

mainLibrary : mainLibrary.cpp book.o
	$(CXX) $(CXXFLAGS) -o $@ $^

book.o : book.cpp book.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean :
	$(RM) *.o
	$(RM) mainLibrary
