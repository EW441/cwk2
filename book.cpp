/*
book.cpp
Author: M00663746
Created: 10/07/2021
Updated: 16/07/2021
*/

#include "book.h"
#include <iostream>
#include <ostream>

/*Book Class*/

//Constructors
Book::Book()
{
  title = "";
  author = "";
  ISBN = "";
  quantity = 0;
}

Book::Book(std::string title, std::string author, std::string ISBN,
	   int quantity)
{
  this->title = title;
  this->author = author;
  this->ISBN = ISBN;
  this->quantity = quantity;
}

//Setter Methods
void Book::setTitle(std::string title)
{
  title = title;
}
void Book::setAuthor(std::string author)
{
  author = author;
}
void Book::setISBN(std::string ISBN)
{
  ISBN = ISBN;
}
void Book::setQuantity(int quantity)
{
  quantity = quantity;
}

//Getter Methods
std::string Book::getTitle()
{
  return title;
}
std::string Book::getAuthor()
{
  return author;
}
std::string Book::getISBN()
{
  return ISBN;
}
int Book::getQuantity()
{
  return quantity;
}

std::ostream& operator<<(std::ostream& out, Book book)
{
  out << book.title << "," << book.author << "," << book.ISBN
      << "," << book.quantity << std::endl;
  return out;
}

