/*
mainLibrary.cpp
Author: M00663746
Created: 10/07/2021
Updated: 16/07/2021
*/

#include <iostream>
#include <fstream>
#include <string>
#include "book.h"

//Linked List Data structure

struct node
{
  Book b1;
  node* next;
};

class LinkedList
{
private:
  node* head, * tail;
public:
  LinkedList()
  {
    head = NULL;
    tail = NULL;
  }

  //Function to add book
  void add_node(Book b1)
  {
    node* tmp = new node;
    tmp->b1 = b1;
    tmp->next = NULL;

    if(head == NULL)
      {
	head = tmp;
	tail = tmp;
      }
    else
      {
	tail->next = tmp;
	tail = tail->next;
      }
  }

  //Function to delete book
  void del_node(std::string title)
  {
    if(head == NULL)
      {
	std::cout << "The list is empty" << std::endl;
      }
    else if(head->next == NULL)
      {
	if(title == head->b1.title)
	  {
	    head = NULL;
	  }
      }
    else if(head->next->b1.title == title)
      {
	head->next = head->next->next;
      }
    else if(head->next != NULL)
      {
	node* temp;
	temp = head;
	while(temp != NULL)
	  {
	    if(temp->next->b1.title == title)
	      {
		break;
	      }
	    temp = temp->next;
	  }
	temp->next = temp->next->next;
	if(temp->next == NULL)
	  {
	    tail = temp;
	  }
      }
  }

  node* gethead()
  {
    return head;
  }

  //Function to display the books
  static void display(node* head)
  {
    if(head == NULL)
      {
	std::cout << "No books to display" << std::endl << std::endl;
      }
    else
      {
	std::cout << "The current books are: " << std::endl << std::endl;
	std::cout << "Title: " <<  head->b1.title << std::endl;
	std::cout << "Author(s): " << head->b1.author << std::endl;
	std::cout << "ISBN: " << head->b1.ISBN << std::endl;
	std::cout << "Qty: " << head->b1.quantity << std::endl << std::endl;
	display(head->next);
      }
  }

  //Function to search books
  void searchBook(std::string title)
  {
    node* temp = head;
    while(temp != NULL)
      {
	if(title == temp->b1.title)
	  {
	    std::cout << "Book Found" << std::endl << std::endl;
	    std::cout << "Title: " << temp->b1.title << std::endl;
	    std::cout << "Author(s): " << temp->b1.author << std::endl;
	    std::cout << "ISBN: " << temp->b1.ISBN << std::endl;
	    std::cout << "Quantity: " << temp->b1.quantity << std::endl;
	  }
	temp = temp->next;
      }
  }	    


  //Function to remove books with 0 quantity
  void removeBook()
  {
    node* temp = head;
    while(temp != NULL)
      {
	if(temp->b1.quantity == 0)
	  {
	    del_node(temp->b1.title);
	  }
	temp = temp->next;
      }
  }
};

//Function to retrieve data from file
void readData(LinkedList& a)
{
  Book b1;
  std::ifstream fin("Books.txt");

  while(!fin.eof())
    {
      getline(fin, b1.title, ';');
      getline(fin, b1.author, ';');
      getline(fin, b1.ISBN, ';');
      fin >> b1.quantity;
      fin.ignore();
      a.add_node(b1);
    }
}

//Function to add new book object
void input(LinkedList& a)
{
  Book ab;
  std::cin.ignore();
  std::cout << "Enter book title: ";
  getline(std::cin, ab.title);
  std::cout << "Enter book author(s): ";
  getline(std::cin, ab.author);
  std::cout << "Enter book ISBN: ";
  getline(std::cin, ab.ISBN);
  std::cout << "Enter quantity: ";
  std::cin >> ab.quantity;
  std::cout << "You have successfully added a book" << std::endl << std::endl;
  a.add_node(ab);
}

  

//Main
int main()
{
  int option = 0;
  LinkedList a;
  std::string title;
  
  std::cout << "----- Welcome to the Library-----" << std::endl << std::endl;


  do
    {
      std::cout << "1. Add book" << std::endl;
      std::cout << "2. Search for book" << std::endl;
      std::cout << "3. Remove book" << std::endl;
      std::cout << "4. Remove book with 0 quantity" << std::endl;
      std::cout << "5. Display books" << std::endl;
      std::cout << "0. EXIT" << std::endl << std::endl;

      std::cin >> option;

      if(option == 1)
	{
	  input(a);
	}
      else if(option == 2)
	{
	  std::cout << "Enter title: ";
	  std::cin >> title;
	  a.searchBook(title);
	}
      else if(option == 3)
	{
	  std::cout << "Enter title: ";
	  std::cin >> title;
	  a.del_node(title);
	}
      else if(option == 4)
	{
	  a.removeBook();
	  std::cout << "Books removed" << std::endl << std::endl;
	}
      else if(option == 5)
	{
	  a.display(a.gethead());
	  //readData(a);
	}
      else
	{
	  std::cout << "Exiting library" << std::endl;
	  break;
	}
    } while (option != 0);    


  return 0;
}
